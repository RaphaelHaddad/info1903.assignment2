-- Write your SQL code to load the tables from your files
-- The first line you need is given below

USE assignment2;

LOAD DATA LOCAL INFILE "wnsynsets.data" 
INTO TABLE wnsynsets 
FIELDS TERMINATED BY " " (num_id, synset);

LOAD DATA LOCAL INFILE "wnnames.data" 
INTO TABLE wnnames 
FIELDS TERMINATED BY "\t" (symbol, descr);

LOAD DATA LOCAL INFILE "wnnames.data" 
INTO TABLE wnnames 
FIELDS TERMINATED BY "\t" (symbol, descr);

LOAD DATA LOCAL INFILE "wnrelations.data" 
INTO TABLE wnrelations 
FIELDS TERMINATED BY " " (id_1, rel, id_2);

LOAD DATA LOCAL INFILE "muc.data" 
INTO TABLE muc 
FIELDS TERMINATED BY " " (word, pos, net, fx);

LOAD DATA LOCAL INFILE "en2es.data" 
INTO TABLE en2es 
FIELDS TERMINATED BY "\t" (en, es);
