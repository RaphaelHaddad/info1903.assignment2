CREATE DATABASE assignment2;

USE assignment2;

CREATE TABLE wnsynsets (
	num_id VARCHAR(8)  NOT NULL,
	synset VARCHAR(225)  NOT NULL,
	PRIMARY KEY (num_id)
);

CREATE TABLE wnnames (
	symbol VARCHAR(2)  NOT NULL,
	descr VARCHAR(225)  NOT NULL,
	PRIMARY KEY (symbol)
);


CREATE TABLE wnrelations (
	id_1 VARCHAR(8)  NOT NULL,
	rel VARCHAR(2)  NOT NULL,
	id_2 VARCHAR(8)  NOT NULL,
	FOREIGN KEY (id_1) REFERENCES wnsynsets (num_id),
	FOREIGN KEY (rel) REFERENCES wnnames (symbol),
	FOREIGN KEY (id_2) REFERENCES wnsynsets (num_id)  
);

CREATE TABLE muc (
	word VARCHAR(225)  NOT NULL,
	pos VARCHAR(50)  NOT NULL,
	net VARCHAR(50)  NOT NULL,
	fx INT UNSIGNED NOT NULL
);

CREATE TABLE en2es (
	en VARCHAR(225)  NOT NULL,
	es VARCHAR(225)  NOT NULL,
	INDEX (en)
);

/*
Query OK, 1 row affected (0.00 sec)

Database changed
Query OK, 0 rows affected (0.01 sec)

Query OK, 0 rows affected (0.00 sec)

Query OK, 0 rows affected (0.00 sec)

Query OK, 0 rows affected (0.01 sec)

Query OK, 0 rows affected (0.00 sec)
*/
