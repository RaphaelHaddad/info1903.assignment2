-- Write your SQL query here.
SELECT s1.synset hyponyms_for_company 
FROM wnrelations r
	INNER JOIN wnsynsets s1 
	on s1.num_id=r.id_2
	INNER JOIN wnsynsets s 
	on s.num_id=r.id_1 and s.synset="company"	
	INNER JOIN wnnames n 
	ON n.symbol=r.rel and n.descr="hyponym";
/*
+------------------------+
| hyponyms_for_company   |
+------------------------+
| broadcasting_company   | 
| bureau_de_change       | 
| car_company            | 
| dot-com                | 
| drug_company           | 
| East_India_Company     | 
| electronics_company    | 
| film_company           | 
| food_company           | 
| furniture_company      | 
| mining_company         | 
| shipping_company       | 
| steel_company          | 
| subsidiary_company     | 
| transportation_company | 
| distributor            | 
| oil_company            | 
| packaging_company      | 
| pipeline_company       | 
| printing_concern       | 
| corporate_investor     | 
| target_company         | 
| white_knight           | 
| limited_company        | 
| holding_company        | 
| service                | 
| livery_company         | 
| open_shop              | 
| closed_shop            | 
| union_shop             | 
| stock_company          | 
| joint-stock_company    | 
| record_company         | 
| mover                  | 
| think_tank             | 
| shipper                | 
| attendance             | 
| cohort                 | 
| number                 | 
| opera_company          | 
| theater_company        | 
| ballet_company         | 
| chorus                 | 
| circus                 | 
| minstrel_show          | 
| minstrelsy             | 
| trainband              | 
| freemasonry            | 
+------------------------+
48 rows in set (0.74 sec)
*/	

