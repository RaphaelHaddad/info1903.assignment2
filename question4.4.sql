-- Write your SQL query here.


SELECT synset
FROM wnsynsets 
WHERE num_id = ANY
		(SELECT num_id
		FROM wnsynsets
		WHERE synset = "dog")
		AND synset != "dog";

-- I interpreted this as finding all the synonyms where the num_id is the same as the two dog num_ids, this query will take a long time because there is none 

-- Empty set (1.12 sec)
-- worste case is executed
