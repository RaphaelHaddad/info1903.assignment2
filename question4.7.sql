-- Write your SQL query here.

SELECT s1.synset as meronyms_that_car_and_truck_share_in_common, count(s1.synset) c 
FROM wnrelations r
	INNER JOIN wnsynsets s 
	on s.num_id=r.id_1 and (s.synset="car" or s.synset="truck")
	INNER JOIN wnsynsets s1 
	on s1.num_id=r.id_2
	INNER JOIN wnnames n 
	ON n.symbol=r.rel and n.descr="part meronym"
GROUP BY s1.synset
HAVING c=2;

/*
+---------------------------------------------+---+
| meronyms_that_car_and_truck_share_in_common | c |
+---------------------------------------------+---+
| bumper                                      | 2 | 
| roof                                        | 2 | 
| stabilizer_bar                              | 2 | 
+---------------------------------------------+---+
3 rows in set (0.08 sec)
*/
