# Write your Python script to process the Message
# Understanding Conference (MUC) data here
dict = {}
############################### Remove tags #####################################
def removetags(triples):
	if triples[2][:2] == 'I-' or triples[2][:2] == 'B-':
		triples[2] = triples[2][2:]
############################### Remove tags #####################################

########################## Counting the Entries #################################
def countentry(triples):
	if triples in dict:
		dict[triples] += 1
	else: 
		dict[triples] = 1
########################## Counting the Entries #################################

####################### Making the word lowercase ###############################
def makelowercase(triples):
	triples[0] = triples[0].lower()
	return triples
####################### Making the word lowercase ###############################


##################### Converting entry back to string ###########################
def convertstring(triples):
	str = ''
	for token in triples:
		str += token + ' '
	return str[:-1]
##################### Converting entry back to string ###########################


for entry in file('muc.txt','rU').read().split():
	triples = makelowercase(entry.split('|'))
	removetags(triples)
	triples	= convertstring(triples)
	countentry(triples)

words = dict.keys()
words.sort()
for word in words:#printing out the dictionary as a string
	print word, str(dict[word])
