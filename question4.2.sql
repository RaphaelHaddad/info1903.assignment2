-- Write your SQL query here.
-- note: mysql is case sensative by default
SELECT count(es) Number_of_spanish_that_are_the_same_as_english_with_o_on_the_end 
FROM en2es 
WHERE concat(en,'o') = es;


/*
+------------------------------------------------------------------+
| Number_of_spanish_that_are_the_same_as_english_with_o_on_the_end |
+------------------------------------------------------------------+
|                                                              573 | 
+------------------------------------------------------------------+
1 row in set (0.06 sec)
*/


