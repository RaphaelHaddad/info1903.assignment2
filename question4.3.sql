-- Write your SQL query here.

/*question stated 4 letter word not words. So i assumed there will only be 1 word with the max frequency it also didn't state to only print out the word. so i printed out
the count and the word */

SELECT word, COUNT(word) wcount  
FROM muc 
WHERE CHAR_LENGTH(word) = 4 
GROUP BY word 
ORDER BY wcount desc 
LIMIT 1;


/*
The most frequent 4 letter word is that with a word count of 7

+------+--------+
| word | wcount |
+------+--------+
| that |      7 | 
+------+--------+
1 row in set (0.01 sec)


The word is 'that'
*/
