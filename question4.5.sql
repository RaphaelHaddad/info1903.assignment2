-- Write your SQL query here.
-- question stated common noun not nouns. So I assumed there will be one word with the most frequency
SELECT es spanish_for_the_most_frequent_English_singular_common_noun 
FROM en2es 
WHERE en= (
		SELECT m.word 
		FROM muc m 
		WHERE m.pos='NN' and m.net='o' 
		ORDER BY m.fx DESC limit 1
	);

/*

+------------------------------------------------------------+
| spanish_for_the_most_frequent_English_singular_common_noun |
+------------------------------------------------------------+
| avion                                                      | 
| plano                                                      | 
| cepillo                                                    | 
| cepillar                                                   | 
+------------------------------------------------------------+
4 rows in set (0.02 sec)
*/
